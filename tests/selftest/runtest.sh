#!/bin/bash
# vim: dict=/usr/share/rhts-library/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of
#   Description: Test for bz497830 ('df' doesn't wait for automount with indirect maps)
#   Author: Jan Scotka <jscotka@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2009 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/share/rhts-library/rhtslib.sh

PACKAGE="python3-dmidecode"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        tar xzvf unit-tests.tar.gz
echo "
import dmidecode

dmidecode.clear_warnings() # Ignore warnings related to no SMBIOS found
dmidecode.set_dev('/tmp/dump.dat')
x = dmidecode.dmidecodeXML()
x.SetResultType(dmidecode.DMIXML_DOC)
x.QuerySection('processor').saveFormatFileEnc('-','UTF-8',1)
" > /tmp/test.py

/bin/cp -f dump.dat /tmp/dump.dat


    rlPhaseEnd

    rlPhaseStartTest
	rlRun "python3 /tmp/test.py"
        cd unit-tests
        rlRun "make test"
    rlPhaseEnd

    rlPhaseStartCleanup
       rm /tmp/test.py
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
